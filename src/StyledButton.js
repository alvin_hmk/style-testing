import * as React from 'react';
import styled from 'styled-components';
import { Button } from 'antd';

const StyledButton = (props) => {
  const { state, setState } = props;

  const btnOnClick = () => {
    setState(!state);
  }

  return <StyledMUIButton onClick={btnOnClick}>
    Change
  </StyledMUIButton>;
}

export default StyledButton;

const StyledMUIButton = styled(Button)`
  font-size: ${({ theme }) => theme.idiotFontSize};
  font-family: ${({ theme }) => theme.madFontFamily};
  color: ${({ theme }) => theme.crazyColor};
  background-color: ${({ theme }) => theme.fooBackgroundColor};

  :hover {
    color: ${({ theme }) => theme.crazyHoverColor};
  }
`;