import * as React from 'react';
import styled from 'styled-components';
import { Divider } from 'antd';

import {
  IndicatorStyledFrame,
  WidgetTitle,
  IndicatorWidgetContentOptimalText,
  IndicatorWidgetContentCurrentText,
} from './Indicator.Styled.Frame';

const IndicatorBComponent = () => {
  return (
    <IndicatorContainer>
      <IndicatorStyledFrame>
        <WidgetTitle>Chiller Plant Power</WidgetTitle>
        <FlexColumnDiv>
          <IndicatorWidgetContentCurrentText><b>480 kW</b></IndicatorWidgetContentCurrentText>
          <StyledlDivider type='horizontal' />
          <IndicatorWidgetContentOptimalText><b>400 kW</b></IndicatorWidgetContentOptimalText>
        </FlexColumnDiv>
      </IndicatorStyledFrame>
    </IndicatorContainer>
  )
}

export default IndicatorBComponent;

const IndicatorContainer = styled.div`
  height: 150px; 
  width: 300px;
`;

const FlexColumnDiv = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  width: 60%;
`;

const StyledlDivider = styled(Divider)`
  margin: 10px 0;
`