import styled from 'styled-components';

export const IndicatorStyledFrame = styled.div`
  background-color: white;
  width: 100%;
  height: 100%;
  border-radius: 2px;
  position: relative;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  flex: 1;
  justify-content: space-around;
  align-items: center;
  border: #777;
  box-shadow: 2px 2px 2px 0 rgba(125, 125, 125, 0.5);
  min-height: 100px;
  overflow: hidden;
`;

export const WidgetTitle = styled.h1`
  position: relative;
  width: 50%;
  color: #3383A4;
  font-weight: 600;
  font-size: calc(100% * 0.75);
  font-family: sans-serif;
`;

export const IndicatorWidgetContentText = styled.h1`
  font-family: sans-serif;
  position: relative;
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  font-weight: 400;
  max-weight: 100%;
  font-size: 36px;
  color: lightgreen;
  margin: 0;
`;