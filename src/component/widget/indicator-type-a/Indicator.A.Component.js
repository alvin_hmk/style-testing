import * as React from 'react';
import styled from 'styled-components';

import {
  IndicatorStyledFrame,
  WidgetTitle,
  IndicatorWidgetContentText
} from './Indicator.Styled.Frame';

const IndicatorAComponent = () => {
  return (
    <IndicatorContainer>
      <IndicatorStyledFrame>
        <WidgetTitle>Optimal Power Saving</WidgetTitle>
        <IndicatorWidgetContentText><b>17%</b></IndicatorWidgetContentText>
      </IndicatorStyledFrame>
    </IndicatorContainer>
  )
}

export default IndicatorAComponent;

const IndicatorContainer = styled.div`
  height: 150px; 
  width: 200px;
`;
