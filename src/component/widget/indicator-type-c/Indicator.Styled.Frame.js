import styled from 'styled-components';

export const IndicatorStyledFrame = styled.div`
  background-color: white;
  width: 100%;
  height: 100%;
  border-radius: 2px;
  position: relative;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-evenly;
  align-items: center;
  border: #777;
  box-shadow: 2px 2px 2px 0 rgba(125, 125, 125, 0.5);
  min-height: 100px;
  overflow: hidden;
`;

export const WidgetTitle = styled.h1`
  position: relative;
  color: #3383A4;
  font-weight: 600;
  font-size: calc(100% * 0.75);
  font-family: sans-serif;
  width: 50%;
`;

export const IndicatorWidgetContentCurrentText = styled.h1`
  font-family: sans-serif;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-weight: 400;
  font-size: calc(100% * 0.95);
  color: blue;
  margin: 0;
  width: 50%;
`;