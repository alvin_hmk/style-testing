import * as React from 'react';
import styled from 'styled-components';
import { Divider } from 'antd';

import {
  IndicatorStyledFrame,
  WidgetTitle,
  IndicatorWidgetContentCurrentText,
} from './Indicator.Styled.Frame';

const IndicatorCComponent = () => {
  return (
    <IndicatorContainer>
      <IndicatorStyledFrame>
        <FlexColumnDiv>
          <WidgetTitle>Outdoor D.B. Temp.</WidgetTitle>
          <IndicatorWidgetContentCurrentText><b>33 °C</b></IndicatorWidgetContentCurrentText>
        </FlexColumnDiv>
        <StyledlDivider type='horizontal' />
        <FlexColumnDiv>
          <WidgetTitle>Outdoor R.H.</WidgetTitle>
          <IndicatorWidgetContentCurrentText><b>70 %</b></IndicatorWidgetContentCurrentText>
        </FlexColumnDiv>
        <StyledlDivider type='horizontal' />
        <FlexColumnDiv>
          <WidgetTitle>Cooling Load</WidgetTitle>
          <IndicatorWidgetContentCurrentText><b>1680 kW</b></IndicatorWidgetContentCurrentText>
        </FlexColumnDiv>
      </IndicatorStyledFrame>
    </IndicatorContainer>
  )
}

export default IndicatorCComponent;

const IndicatorContainer = styled.div`
  height: 250px;
  width: 550px;
`;

const FlexColumnDiv = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  justify-content: space-evenly;
  width: 100%;
`;

const StyledlDivider = styled(Divider)`
  margin: 0;
`