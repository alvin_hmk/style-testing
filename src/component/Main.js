import * as React from 'react';

// import logo from '../logo.svg';

import styled from 'styled-components';
import { useState } from 'react';
import { ThemeProvider } from 'styled-components';
import { useLazyQuery } from '@apollo/client';
import { Button, Divider } from 'antd';

import StyledButton from '../StyledButton';
import { GET_STYLE } from './query/query';
import IndicatorAComponent from './widget/indicator-type-a/Indicator.A.Component';
import IndicatorBComponent from './widget/indicator-type-b/Indicator.B.Component';
import IndicatorCComponent from './widget/indicator-type-c/Indicator.C.Component';
import LatestMessage from './Subscription.Component';

const Main = () => {
  const [state, setState] = useState(false);
  const [gqlTheme, setGqlTheme] = useState({});
  const [getStyle, { loading, error }] = useLazyQuery(GET_STYLE, {
    fetchPolicy: 'no-cache',
  })

  React.useEffect(() => {
    getStyle().then((result) => {
      console.log(result);
      setGqlTheme(result.data.getStyle);
    });
  }, []);

  const manualGetStyle = () => {
    getStyle().then((result) => {
      console.log(result);
      setGqlTheme(result.data.getStyle);
    });
  };

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error : {error.message}</p>;

  return (
    <ThemeProvider theme={gqlTheme}>
      <div className="App">
        <StyledHeader className="App-header" state={state}>
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <StyledP state={state}>
            Edit <code>src/App.js</code> and save to reload.
          </StyledP>
          <StyledA
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </StyledA>
          <StyledButton state={state} setState={setState} />
          <LatestMessage />
          <Button onClick={manualGetStyle}>Get Style</Button>
          <br />
          <StyledAnotherHeader>
            <IndicatorAComponent />
            <VerticalDivider type='vertical' />
            <IndicatorBComponent />
            <VerticalDivider type='vertical' />
            <IndicatorCComponent />
          </StyledAnotherHeader>
        </StyledHeader>
      </div>
    </ThemeProvider>
  )
}

export default Main;

const StyledHeader = styled.header`
  background-color: ${({ state, theme }) => state
    ? theme.hororBackgroundColor
    : theme.invertHororBackgroundColor
  };
`;

const StyledAnotherHeader = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const VerticalDivider = styled(Divider)``;

const StyledP = styled.p`
  color: ${({ state, theme }) => state
    ? theme.disgustPColor
    : theme.invertDisgustPColor
  };
  font-family: ${({ theme }) => theme.madFontFamily};
`;

const StyledA = styled.a`
  font-family: ${({ theme }) => theme.madFontFamily};
`
