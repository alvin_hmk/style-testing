import * as React from 'react';
import styled from 'styled-components';
import { useSubscription } from '@apollo/client';

import { SUBSCIPTION_TEST } from './query/query';

const LatestMessage = () => {
  const { data, loading: sub_loading } = useSubscription(SUBSCIPTION_TEST);
  console.log(data, sub_loading)
  return <h4><StyledP>{JSON.stringify(data)}</StyledP></h4>
}

export default LatestMessage;

const StyledP = styled.p`
  color: ${({ state, theme }) => state
    ? theme.disgustPColor
    : theme.invertDisgustPColor
  };
  font-family: ${({ theme }) => theme.madFontFamily};
`;
