import { gql } from '@apollo/client';

export const GET_STYLE = gql`
    query {
        getStyle {
        crazyColor
        crazyHoverColor
        fooBackgroundColor
        disgustPColor
        hororBackgroundColor
        idiotFontSize
        madFontFamily
        invertDisgustPColor
        invertHororBackgroundColor
        }
    }
`;

export const SUBSCIPTION_TEST = gql`
    subscription newMessage {
        newMessage {
            message
        }
    }
`;
