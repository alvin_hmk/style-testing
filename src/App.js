import './App.css';

import * as React from 'react';
import { ApolloProvider } from '@apollo/client';

import client from './client/ApolloClient';
import Main from './component/Main';

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Main />
    </ApolloProvider>
  );
}

export default App;
