const blueTheme = {
    hororBackgroundColor: 'white',
    invertHororBackgroundColor: 'black',

    disgustPColor: 'black',
    invertDisgustPColor: 'white',

    crazyColor: 'red',
    idiotFontSize: '15px',
    damnBackgroundColor: 'rgba(0, 0, 0, 0)',
    crazyHoverColor: 'blue',
}

const orangeTheme = {
    hororBackgroundColor: 'white',
    invertHororBackgroundColor: 'darkred',

    disgustPColor: 'black',
    invertDisgustPColor: 'white',

    crazyColor: 'red',
    idiotFontSize: '15px',
    damnBackgroundColor: 'rgba(0, 0, 0, 0)',
    crazyHoverColor: 'blue',
}

const fontMapper = {
    'small': '10px',
    'medium': '15px',
    'large': '20px',
}

const themeMapper = {
    'blue': blueTheme,
    'orange': orangeTheme,
}

const themeSelect = (profile) => {
    return themeMapper[profile]
}

export default themeSelect;